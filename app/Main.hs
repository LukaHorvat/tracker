module Main where

import Poll
import UI
import Control.Concurrent
import Control.Monad

main :: IO ()
main = do
    void $ forkIO uiThread
    pollLoop
