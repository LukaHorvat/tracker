module Database.SQLite.Simple.Streaming (streamQuery, streamQuery_) where

import Database.SQLite.Simple
import Control.Monad.IO.Class
import Streaming
import qualified Streaming.Prelude as Stream
import Control.Monad

streamQuery :: (ToRow params, FromRow a, MonadIO m)
    => Connection -> Query -> params -> Stream (Of a) m ()
streamQuery c q p = do
    stmt <- liftIO $ openStatement c q
    liftIO $ bind stmt p
    loop stmt
    liftIO $ closeStatement stmt

streamQuery_ :: (FromRow a, MonadIO m) => Connection -> Query -> Stream (Of a) m ()
streamQuery_ c q = do
    stmt <- liftIO $ openStatement c q
    loop stmt
    liftIO $ closeStatement stmt

loop :: (MonadIO m, FromRow a) => Statement -> Stream (Of a) m ()    
loop stmt = do
    mr <- liftIO $ nextRow stmt
    forM_ mr $ \r -> do
        Stream.yield r
        loop stmt
