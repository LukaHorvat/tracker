module UI where

import Common
import Graphics.UI.Threepenny (UI, Element, (#+))
import qualified Graphics.UI.Threepenny as TP
import Analyze
import qualified Data.Map as Map

uiThread :: IO ()
uiThread = TP.startGUI TP.defaultConfig $ \w -> do
    styling (TP.getHead w)
    resultMap <- fetchResults atomTgt
    let gr = resultMap
            & Map.toList
            & Prelude.map (\(Target{..}, s) ->
                [TP.string (toS processName), TP.string (toS targetData), TP.string (show s)])
            & gridTable [TP.string "Process name", TP.string "Data", TP.string "Seconds"]
    void $ TP.getBody w #+ [TP.div #+ [gr]]
    void $ TP.getBody w & TP.set TP.style [("padding", "1em")]

for :: [a] -> (a -> b) -> [b]
for = flip Prelude.map

gridTable :: [UI Element] -> [[UI Element]] -> UI Element
gridTable header mrows = TP.table #+
    [ TP.mkElement "thead" #+ makeRows TP.th [header]
    , TP.mkElement "tbody" #+ makeRows TP.td mrows ]
    where
    makeRows cell elems = UI.for elems $ \row0 ->
        let row' = UI.for row0 $ \entry' -> cell #+ [entry']
        in TP.tr #+ row'

styling :: UI Element -> UI ()
styling ue = void $ ue #+
    [ TP.link
        & TP.set TP.rel "stylesheet"
        & TP.set TP.href "//fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic"
    , TP.link
        & TP.set TP.rel "stylesheet"
        & TP.set TP.href "//cdn.rawgit.com/necolas/normalize.css/master/normalize.css"
    , TP.link
        & TP.set TP.rel "stylesheet"
        & TP.set TP.href "//cdn.rawgit.com/milligram/milligram/master/dist/milligram.min.css" ]
