CREATE TABLE record (
    time timestamp not null default current_timestamp,
    window_title text,
    process_name text,
    time_inactive int not null
);
