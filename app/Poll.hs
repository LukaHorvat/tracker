{-# LANGUAGE OverloadedStrings #-}
module Poll where

import Common
import Control.Concurrent
import Focus
import Database.SQLite.Simple

pollLoop :: IO ()
pollLoop = do
    conn <- liftIO (open "db")
    forever $ do
        mfocus :: Maybe FocusedStatus <- implementFocusViaExecutable "wnd" getFocusedStatus
        params <- case mfocus of
            Nothing -> return (Nothing, Nothing, 0)
            Just FocusedStatus{..} -> return (Just title, Just processName, timeInactive)
        print params
        liftIO $ execute conn
            "insert into record (window_title, process_name, time_inactive) \
            \values (?, ?, ?)"
            params
        liftIO $ threadDelay 1000000
