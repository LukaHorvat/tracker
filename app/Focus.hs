{-# LANGUAGE ViewPatterns #-}
module Focus where

import Common
import Control.Effects
import System.Process
import System.Exit

data FocusedStatus = FocusedStatus
    { title :: Text
    , processName :: Text
    , timeInactive :: Int }
    deriving (Eq, Ord, Read, Show, Generic)

data Focus

instance Effect Focus where
    data EffMethods Focus m = FocusMethods
        { _getFocusedStatus :: m (Maybe FocusedStatus) }
        deriving Generic

getFocusedStatus :: MonadEffect Focus m => m (Maybe FocusedStatus)
FocusMethods getFocusedStatus = effect

readOptional :: String -> Maybe String
readOptional ('1' : ' ' : rest) = Just rest
readOptional _ = Nothing

implementFocusViaExecutable :: MonadIO m => FilePath -> RuntimeImplemented Focus m a -> m a
implementFocusViaExecutable fp = implement $ FocusMethods $ liftIO $ do
    (code, out, err) <- readProcessWithExitCode fp [] ""
    case (code, fmap readOptional (lines out)) of
        (ExitSuccess, [Just (toS -> procName), Just (toS -> mdata), Just (read -> time)])
            -> return (Just (FocusedStatus procName mdata time))
        (ExitSuccess, _) -> return Nothing
        (_, _) -> ioError (userError err)
