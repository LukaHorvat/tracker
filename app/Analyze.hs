{-# LANGUAGE OverloadedStrings, DuplicateRecordFields #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}
module Analyze where

import Common
import Database.SQLite.Simple.Streaming
import Database.SQLite.Simple
import Data.Time
import qualified Streaming.Prelude as Stream
import Streaming (Stream, Of)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Foldable (toList)
import Network.URI

data Record = Record
    { time :: UTCTime
    , windowTitle :: Maybe Text
    , processName :: Maybe Text
    , timeInactive :: Int }
    deriving (Eq, Ord, Read, Show, Generic)

instance FromRow Record where
    fromRow = Record <$> field <*> field <*> field <*> field

fetchAll :: MonadIO m => Stream (Of Record) m ()
fetchAll = do
    conn <- liftIO $ open "db"
    streamQuery_ conn "select * from record order by time asc"

pairwise :: Monad m => (a -> a -> b) -> Stream (Of a) m r -> Stream (Of b) m r
pairwise f =
    Stream.map (uncurry f)
    . Stream.mapMaybe (\s -> do
        [a, b] <- return $ toList s
        return (a, b))
    . Stream.slidingWindow 2

pairwiseFirst :: Monad m => (a -> b) -> (a -> a -> b) -> Stream (Of a) m () -> Stream (Of b) m ()
pairwiseFirst f g =
    pairwise (\ma mb -> case (ma, mb) of
        (Just a, Just b) -> g a b
        (Nothing, Just a) -> f a
        _ -> error "Impossible")
    . (Stream.yield Nothing <>)
    . Stream.map Just

data FocusedWindow = FocusedWindow
    { title :: Text
    , processName :: Text }

data Interval = Interval
    { active :: Bool
    , focusedWindow :: Maybe FocusedWindow
    , endTime :: UTCTime
    , startTime :: UTCTime }

recordsToIntervals :: Monad m => Stream (Of Record) m r -> Stream (Of Interval) m r
recordsToIntervals = pairwise f
    where
    f r1 r2 = let d = diffUTCTime (time r2) (time r1) in Interval
        (d < 10 && Analyze.timeInactive r2 <= floor (d * 1000))
        (case (windowTitle r2, Analyze.processName (r2 :: Record)) of
            (Just wt, Just pn) -> Just (FocusedWindow wt pn)
            _ -> Nothing)
        (time r2)
        (time r1)

data TargetDesc = TargetDesc
    { processName :: Text
    , dataParser :: Text -> Maybe Text }

data Target = Target
    { processName :: Text
    , targetData :: Text }
    deriving (Eq, Ord, Read, Show)

awayLimit :: Num a => a
awayLimit = 10 * 60

data TargetInterval = TargetInterval
    { target :: Target
    , startTime :: UTCTime
    , endTime :: UTCTime }

targets :: Monad m => [TargetDesc]
    -> Stream (Of Interval) m r
    -> Stream (Of TargetInterval) m r
targets ts = Stream.catMaybes . Stream.map f
    where
    mp = Map.fromList (map ((,) <$> (processName :: TargetDesc -> _) <*> dataParser) ts)
    f Interval{..}
        | not active = Nothing
        | otherwise = case getTarget focusedWindow of
            Just t -> Just (TargetInterval t startTime endTime)
            Nothing -> Nothing
    getTarget Nothing = Nothing
    getTarget (Just FocusedWindow{..}) = case Map.lookup processName mp of
        Nothing -> Nothing
        Just pars -> Target processName <$> pars title

extendLeft :: Monad m => Stream (Of TargetInterval) m () -> Stream (Of TargetInterval) m ()
extendLeft = pairwiseFirst id
    (\TargetInterval{ endTime = et } t2@TargetInterval{ startTime = st } ->
        if diffUTCTime st et < awayLimit
            then (t2 :: TargetInterval) { startTime = et }
        else t2)

collectResults :: Monad m => Stream (Of TargetInterval) m r -> m (Map Target Integer)
collectResults =
    Stream.fold_ (\mp (TargetInterval t s e) ->
        Map.insertWith (+) t (floor $ diffUTCTime e s) mp) Map.empty id

fetchResults :: MonadIO m => [TargetDesc] -> m (Map Target Integer)
fetchResults ts = fetchAll
    & recordsToIntervals
    & targets ts
    & extendLeft
    & collectResults

atomTgt :: [TargetDesc]
atomTgt = [TargetDesc "XD" (fmap (toS . unEscapeString . uriPath) . parseAbsoluteURI . toS)]
